// https://dev.to/finiam/predictable-react-authentication-with-the-context-api-g10

import React, { createContext, useContext, useEffect, useMemo, useState } from "react";

import {
    CognitoUserPool,
    CognitoUserAttribute,
    CognitoUser,
    AuthenticationDetails,
    CognitoUserSession
} from 'amazon-cognito-identity-js';


// const POOL_DATA = {
//     UserPoolId: '',
//     ClientId: ''
// };

import POOL_DATA from "./POOL_DATA";

const userPool = new CognitoUserPool(POOL_DATA);

// interface AuthContextType {
//     // We defined the user type in `index.d.ts`, but it's
//     // a simple object with email, name and password.
//     user?: User;
//     loading: boolean;
//     error?: any;
//     login: (email: string, password: string) => void;
//     signUp: (email: string, name: string, password: string) => void;
//     logout: () => void;
//   }

const AuthContext = createContext({});

export function AuthProvider({ children }) {
    const [session, setSession] = useState();
    const [error, setError] = useState();
    const [loading, setLoading] = useState(false); // TODO test with out
    const [loadingInitial, setLoadingInitial] = useState(true);

    console.log({ session });

    const pullSession = () => new Promise((resolve, reject) => {
        var user = userPool.getCurrentUser()
        if (!user) { 
            return reject("no user");
        }
        user.getSession((err, session) => {
            if (err) { 
                setError(err); 
                return reject();
            }
            setSession(session);
            resolve(session);
        });
    });

    useEffect(() => {
        pullSession().finally(() => setLoadingInitial(false));
    }, []);

    const clearError = () => setError(null);


    const signIn = (username, password) => {
        setLoading(true);

        const authData = {
            Username: username,
            Password: password
        };
        const authDetails = new AuthenticationDetails(authData);
        const userData = {
            Username: username,
            Pool: userPool
        };
        const cognitoUser = new CognitoUser(userData);

        return new Promise((resolve, reject) => {
            cognitoUser.authenticateUser(authDetails, {
                onSuccess(result) {
                    setLoading(false);
                    setError(null);
                    pullSession(); // maybe instead it should be .finally(setLoading(false))
                    resolve();
                },
                onFailure(err) {
                    setLoading(false);
                    setError(err);
                    setSession(null);
                    console.log(err);
                    reject(err);
                }
            });
        });
    };

    const signUp = (username, email, password) => {
        setLoading(true);

        const user = {
            username: username,
            email: email,
            password: password
        };
        const attrList = [];  //: CognitoUserAttribute[]
        const emailAttribute = {
            Name: 'email',
            Value: user.email
        };
        attrList.push(new CognitoUserAttribute(emailAttribute));

        return new Promise((resolve, reject) => {
            userPool.signUp(user.username, user.password, attrList, null, (err, result) => {
                if (err) {
                    setLoading(false);
                    setError(err);
                    return reject();
                }
                setLoading(false);
                setError(null);
                resolve(user);
            });
        })
        
    };

    const confirmUser = (username, code) => {
        setLoading(true);
        const userData = {
            Username: username,
            Pool: userPool
        };
        const cognitUser = new CognitoUser(userData);

        return new Promise((resolve, reject) => {
            cognitUser.confirmRegistration(code, true, (err, result) => {
                if (err) {
                    setLoading(false);
                    setError(err);
                    return reject();
                }
                setLoading(false);
                setError(null);
                pullSession().finally(() => resolve());
            });
        });
    }

    const getUser = () => userPool.getCurrentUser();

    const logout = () => {
        userPool.getCurrentUser()?.signOut();
        setSession(null);
        setError(undefined);
    };

    const memoedValue = useMemo(
        () => ({
          session,
          loading,
          error,
          clearError,
          signIn,
          signUp,
          confirmUser,
          logout,
          getUser
        }),
        [session, loading, error]
      );

      return (
        <AuthContext.Provider value={memoedValue}>
          {!loadingInitial && children}
        </AuthContext.Provider>
      );
}


export default function useAuth() {
    return useContext(AuthContext);
}