import React, { createContext, useContext, useEffect, useMemo, useState } from "react";

import axios from 'axios';

import useAuth from "./auth-context";
import { baseUrl } from "./urls";

const CYContext = createContext({});

export function CYProvider({ children }) {
    const { session } = useAuth();
    
    const [loading, setLoading] = useState(false);
    const [error, setError] = useState(undefined);

    const get = async (all = true) => {
        if (!session) { return Promise.reject("not logged in"); }
        const Authorization = session.idToken.jwtToken;
        const accessToken = session.getAccessToken().getJwtToken();
        const queryParam = '?accessToken=' + accessToken;
        let urlParam = 'all';
        if (!all) {
            urlParam = 'single';
        }
        setLoading(true);
        return axios.get(baseUrl + "/" + urlParam + queryParam, {
            headers: { Authorization }
        })
            .finally(() => setLoading(false))
            .catch(err => setError(err));
    };

    const post = async (body) => {
        if (!session) { return Promise.reject("not logged in"); }
        const Authorization = session.idToken.jwtToken;
        setLoading(true);
        return axios
            .post(baseUrl, body, { headers: { Authorization } })
            .finally(() => setLoading(false))
            .catch(err => setError(err));
    };

    const Delete = async () => {
        if (!session) { return Promise.reject("not logged in"); }
        const Authorization = session.idToken.jwtToken;
        const accessToken = session.getAccessToken().getJwtToken();
        const queryParam = '?accessToken=' + accessToken;

        setLoading(true);
        return axios
            .delete(baseUrl + queryParam, { headers: { Authorization } })
            .finally(() => setLoading(false))
            .catch(err => setError(err));
     };

    const clearError = () => setError();

    const memoedValue = useMemo(
        () => ({ 
            get, post, Delete,
            loading, error, clearError
        }),
        [get, loading, error]
    );

    return (
        <CYContext.Provider value={ memoedValue }>
            { children }
        </CYContext.Provider>
    )
}

export default function useCyApi() {
    return useContext(CYContext);
}