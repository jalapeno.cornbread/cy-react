import { Article, Home, Group } from "@mui/icons-material";

import SignIn from "./components/sign-in";
import SignUp from "./components/sign-up";
import CompareYourselfTable from "./components/table";
import EditUser from "./components/edit-user";

const routes = [
    {
        name: "Sign In",
        component: SignIn,
        icon: Home,
        needsAuth: false
    },
    {
        name: "Sign Up",
        component: () => <SignUp/>,
        icon: Article,
        needsAuth: false
    },
    {
        name: "Table",
        component: () => <CompareYourselfTable/>,
        icon: Group,
        needsAuth: true
    },
    {
        name: "Edit",
        component: () => <EditUser/>,
        icon: Group,
        needsAuth: true
    },
    
];

export default routes;

export var homeRoute = routes[0];