import Sidebar from "./components/Sidebar";
import { Box, createTheme, Stack, ThemeProvider, Paper, Card } from "@mui/material";
import Navbar from "./components/Navbar";
import { useState } from "react";
import { styled } from '@mui/material/styles';

import routes, { homeRoute } from "./routes";
import { AuthProvider } from "./auth-context";
import { CYProvider } from "./cy-api";

// const routes = [
//   "SignIn",
//   "SignUp",
//   "Table",
//   "Edit",
//   "View all"
// ];

// const main_components = [
//   () => <SignIn/>,
//   () => <SignUp/>
// ];

const Item = styled(Paper)(({ theme }) => ({
  backgroundColor: theme.palette.mode === 'dark' ? '#1A2027' : '#fff',
  ...theme.typography.body2,
  padding: theme.spacing(1),
  textAlign: 'center',
  color: theme.palette.text.secondary,
}));

function App() {
  const [mode, setMode] = useState("light");
  const [route, setRoute] = useState(routes[0]);

  const darkTheme = createTheme({
    palette: {
      mode: mode,
    },
  });

  const onChangeRoute = routeName => {
    setRoute(routes.find( r => r.name == routeName ));
  };

  const returnToHome = () => setRoute(homeRoute);

  let main = <route.component  returnToHome={returnToHome} />;
  

  return (
    <ThemeProvider theme={darkTheme}>
      <AuthProvider>
        <CYProvider>
          <Box bgcolor={"background.default"} color={"text.primary"}>
            <Navbar returnToHome={returnToHome} />

            <Stack direction="row" spacing={2} justifyContent="space-between">
              <Sidebar setMode={setMode} mode={mode} onSelect={onChangeRoute} />
              <Box flex={4} p={{ xs: 0, md: 2}}>
                <Card sx={{ padding: 5 }}>
                  { main }

                </Card>
              </Box>
            </Stack>
            
          </Box>
        </CYProvider>
      </AuthProvider>

    </ThemeProvider>
  );
}

export default App;
