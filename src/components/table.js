import React, { useState, useEffect } from "react";

import MUIDataTable from "mui-datatables";
import axios from "axios";

import {baseUrl} from '../urls';
import useAuth from "../auth-context";
import useCyApi from "../cy-api";

const CompareYourselfTable = () => {
  const [rows, setRows] = useState();
  console.log({rows})
  // const { session } = useAuth();

  const { get, Delete } = useCyApi();

  useEffect(() => {
      get()
          .then((res) =>
            setRows(res.data)
          )
          .catch((err) => console.log(err)); 
  }, []); // eslint-disable-line

  const handleDeleteRow = async () => {
    Delete()
      .then((res) => console.log(res))
      .catch((err) => console.log(err));
  };

  const columns = [
    { label: "Age", name: "age" },
    { label: "Height", name: "height" },
    { label: "Income", name: "income" },
  ];

  const options = {
    filterType: "dropdown",
    responsive: "stacked",
    selectableRows: "single",
    onRowsDelete: (rowsDeleted) => {
      //const idToDelete = rowsDeleted.data.map((item) => item.dataIndex);
      handleDeleteRow();
    },
    selectableRowsOnClick: true,
  };

  return (
    <MUIDataTable
      title={"Sample Data"}
      data={
        rows &&
        rows.map((rowObj) => {
          return [
            rowObj.age, 
            rowObj.height, 
            rowObj.income
          ];
        })
      }
      columns={columns}
      options={options}
    />
  );

 
};

export default CompareYourselfTable;
