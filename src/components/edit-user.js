// does not need useAuth().user, just session

import React, { useState, useEffect } from "react";

import { Box, Button, TextField, Typography, Snackbar } from '@mui/material';
import { makeStyles } from "@mui/styles";
import axios from "axios";

import { baseUrl } from "../urls";

import useAuth from "../auth-context";
import useCyApi from "../cy-api";

const useStyles = makeStyles((theme) => ({
    root: {
        "& > *": {
            margin: theme.spacing(1),
            marginTop: theme.spacing(3),
            marginLeft: theme.spacing(5),
            width: "25ch",
        },
    },
    submit: {
        display: "flex",
        width: "5%",
        marginLeft: "10%",
        backgroundColor: "",
    },
}));


const defaultEntry = () => ({
    age: 0,
    income: 0,
    height: 0,
});

const EditUser = () => {
    const classes = useStyles();

    const { get, post } = useCyApi();

    const [newEntry, setNewEntry] = useState(defaultEntry());

    const [loading, setLoading] = useState(true);

    const handleFieldChange = (field, value) => {
        setNewEntry((prev) => ({ ...prev, [field]: value }));
    };

    const handleSubmit = async () => {
        const params = {
            age: Number(newEntry.age),
            income: Number(newEntry.income),
            height: Number(newEntry.height),
        };

        post(params).then((res) => {}
            //window.location.replace("/table").catch((err) => console.log(err))
        );
    };

    const onRetrieveData = (all = true) => {
        return get(false)
            .then(res => {
                const { age, income, height } = res.data[0];
                setNewEntry({
                    age: String(age),
                    income: String(income),
                    height: String(height)
                });
            });
    };

    useEffect(() => {
        onRetrieveData(false)
            .finally(() => setLoading(false));
    }, [])

    if (loading) {
        return (
            <Typography style={{ fontStyle: "italic" }} variant="p">
                Loading user...
            </Typography>
        )
    }

    return (
        <>
            <Typography variant="h3">Edit entry</Typography>
            <Typography style={{ fontStyle: "italic" }} variant="p">
                Modify your stats
            </Typography>
            <Box
                component="form"
                sx={{
                    '& > :not(style)': { m: 1, width: '25ch' },
                }}
                noValidate
                autoComplete="off"
            >
              
                <TextField
                    id="outlined-basic-age-input"
                    label="Age"
                    value={newEntry.age}
                    onChange={(e) => handleFieldChange("age", e.target.value)}
                    variant="outlined"
                    style={{ display: "flex" }}
                />
                <TextField
                    id="outlined-basic-income-input"
                    label="Income"
                    value={newEntry.income}
                    onChange={(e) => handleFieldChange("income", e.target.value)}
                    variant="outlined"
                    style={{ display: "flex" }}
                />
                <TextField
                    id="outlined-basic-height-input"
                    label="Height"
                    value={newEntry.height}
                    onChange={(e) => handleFieldChange("height", e.target.value)}
                    variant="outlined"
                    style={{ display: "flex" }}
                />
                <Button
                    variant="contained"
                    color="primary"
                    className={classes.submit}
                    onClick={() => handleSubmit()}
                >
                    Submit
                </Button>
            </Box>
        </>
    );
};

export default EditUser;
