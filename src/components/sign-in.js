
import React, { useState } from 'react';

import Button from '@mui/material/Button';
import Snackbar from '@mui/material/Snackbar';
import Box from '@mui/material/Box';
import IconButton from '@mui/material/IconButton';
import CloseIcon from '@mui/icons-material/Close';
import Typography from '@mui/material/Typography';
import { TextField } from '@mui/material';
import { makeStyles } from "@mui/styles";
import axios from "axios";
import { baseUrl } from "../urls";
import useAuth from '../auth-context';

//import { AuthService } from "../user/auth-service";
// import { VerifiedUser } from "@material-ui/icons";

// Callback is terrible design, should use context

const useStyles = makeStyles((theme) => ({
    root: {
        "& > *": {
            margin: theme.spacing(1),
            marginTop: theme.spacing(3),
            marginLeft: theme.spacing(5),
            width: "25ch",
        },
    },
    submit: {
        display: "flex",
        width: "5%",
        marginLeft: "10%",
        backgroundColor: "",
    },
}));

// function Alert(props) {
//   return <MuiAlert elevation={6} variant="filled" {...props} />;
// }

const SignIn = () => {
    const classes = useStyles();

    const { signIn, loading } = useAuth();

    const [snackbar, setSnackbar] = useState({ open: false, message: "" });
    const [newEntry, setNewEntry] = useState({
        Username: "",
        Email: "",
        Password: "",
    });

    const handleFieldChange = (field, value) => {
        setNewEntry((prev) => ({ ...prev, [field]: value }));
    };

    const handleSignIn = () => {
        if (loading) { return; }
        signIn(newEntry.Username, newEntry.Password)
            .then(() => setSnackbar({ open: true, message: "Login Successful." }))
            .catch(err => setSnackbar({ open: true, message: "Bad things happened : " + err }));
    }

    const onCloseSnackbar = () => {
        setSnackbar({ open: false, message: "" });
    }

    return (
        <>
            <Snackbar
                open={snackbar.open}
                autoHideDuration={6000}
                onClose={onCloseSnackbar}
                message={snackbar.message}
            />

            <Typography variant="h3">Sign In</Typography>
            <Typography style={{ fontStyle: "italic" }} variant="p">
                Login into your account
            </Typography>
            <Box
                component="form"
                sx={{
                    '& > :not(style)': { m: 1, width: '25ch' },
                }}
                noValidate
                autoComplete="off"
            >
                <TextField
                    id="outlined-basic-username-input"
                    label="Username"
                    value={newEntry.Username}
                    onChange={(e) => handleFieldChange("Username", e.target.value)}
                    variant="outlined"
                    style={{ display: "flex" }}
                />

                <TextField
                    id="outlined-basic-password-input"
                    label="Password"
                    value={newEntry.Password}
                    onChange={(e) => handleFieldChange("Password", e.target.value)}
                    variant="outlined"
                    style={{ display: "flex" }}
                />

                <Button
                    className={classes.submit}
                    variant="contained"
                    color="primary"
                    onClick={() => handleSignIn()}
                >
                    Submit
                </Button>
            </Box>

        </>
    );
};

export default SignIn;
