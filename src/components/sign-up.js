import React, { useState } from "react";

// import MuiAlert from '@material-ui/lab/Alert';

import { Box, Button, TextField, Typography, Snackbar } from '@mui/material';
import { makeStyles } from "@mui/styles";

import axios from "axios";
import { baseUrl } from "../urls";

import useAuth from "../auth-context";

// import { AuthService } from "../user/auth-service";
// import { VerifiedUser } from "@material-ui/icons";

const useStyles = makeStyles((theme) => ({
  // root: {
  //   "& > *": {
  //     margin: theme.spacing(1),
  //     marginTop: theme.spacing(3),
  //     marginLeft: theme.spacing(5),
  //     width: "25ch",
  //   },
  // },
  submit: {
    display: "flex",
    width: "5%",
    marginLeft: "10%",
    backgroundColor: "",
  },
}));

// function Alert(props) {
//   return <MuiAlert elevation={6} variant="filled" {...props} />;
// }

const SignUp = () => {
  const classes = useStyles();

  const { signUp, confirmUser, loading } = useAuth();

  const [snackbar, setSnackbar] = useState({ open: false, message: "" });
  const [entry, setEntry] = useState({
    Username: "",
    Email: "",
    Password: "",
    RePassword: "",
    VerificationCode: ""

  });

  const handleFieldChange = (field, value) => {
    setEntry((prev) => ({ ...prev, [field]: value }));
  };

  const handleSignUp = async () => {

    if (entry.Password !== entry.RePassword) {
      setSnackbar({ open: true, message: "Passwords dont match" });
      return;
    }
    const params = {
      username: entry.Username,
      email: entry.Email,
      password: entry.Password
    };

    signUp(params.username, params.email, params.password)
      .then(user => setSnackbar({ open: true, message: "Success on creating user, check email." }))
      .catch(err => setSnackbar({ open: true, message: "Bad things happened : " + err }));
  };

  const handleConformation = () => {
    confirmUser(entry.Username, entry.VerificationCode)
      .then(() => setSnackbar({ open: true, message: "Success on verification." }))
      .catch(err => setSnackbar({ open: true, message: "Bad things happened : " + err }));
  }

  const onCloseSnackbar = () => {
    setSnackbar({ open: false, message: "" });
  }

  return (
    <>
      <Snackbar
        open={snackbar.open}
        autoHideDuration={6000}
        onClose={onCloseSnackbar}
        message={snackbar.message}
      />

      {loading ?
        (
          <>
            <Typography variant="h3">Sign Up</Typography>
            <Typography style={{ fontStyle: "italic" }} variant="p">
              Accessing server...
            </Typography>
          </>
        ) : (
          <>

            <Typography variant="h3">Sign Up</Typography>
            <Typography style={{ fontStyle: "italic" }} variant="p">
              Create an account
            </Typography>


            <Box
              component="form"
              sx={{
                '& > :not(style)': { m: 1, width: '25ch' },
              }}
              noValidate
              autoComplete="off"
            >
              <TextField
                id="outlined-basic-username-input"
                label="Username"
                value={entry.Username}
                onChange={(e) => handleFieldChange("Username", e.target.value)}
                variant="outlined"
                style={{ display: "flex" }}
              />
              <TextField
                id="outlined-basic-email-input"
                label="Email"
                value={entry.Email}
                onChange={(e) => handleFieldChange("Email", e.target.value)}
                variant="outlined"
                style={{ display: "flex" }}
              />
              <TextField
                id="outlined-basic-password-input"
                label="Password"
                value={entry.Password}
                onChange={(e) => handleFieldChange("Password", e.target.value)}
                variant="outlined"
                style={{ display: "flex" }}
              />
              <TextField
                id="outlined-basic-repassword-input"
                label="Retype Password"
                value={entry.RePassword}
                onChange={(e) => handleFieldChange("RePassword", e.target.value)}
                variant="outlined"
                style={{ display: "flex" }}
              />

              <Button
                variant="contained"
                color="primary"
                className={classes.submit}
                onClick={() => handleSignUp()}
              >
                Submit
              </Button>
            </Box>

            <Box
              component="form"
              sx={{
                '& > :not(style)': { m: 1, width: '25ch' },
              }}
              noValidate
              autoComplete="off"
            >
              <TextField
                id="outlined-basic-username-input"
                label="Username"
                value={entry.Username}
                onChange={(e) => handleFieldChange("Username", e.target.value)}
                variant="outlined"
                style={{ display: "flex" }}
              />

              <TextField
                id="outlined-basic-validation-code-input"
                label="Verification Code"
                value={entry.VerificationCode}
                onChange={(e) => handleFieldChange("VerificationCode", e.target.value)}
                variant="outlined"
                style={{ display: "flex" }}
              />


              <Button
                variant="contained"
                color="primary"
                className={classes.submit}
                onClick={() => handleConformation()}
              >
                Submit
              </Button>
            </Box>
          </>
        )
      }

    </>
  );
};

export default SignUp;
