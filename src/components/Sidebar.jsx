import React from "react";

import {
  Box,
  List,
  ListItem,
  ListItemButton,
  ListItemIcon,
  ListItemText,
  Switch,
} from "@mui/material";

import {
  ModeNight,
} from "@mui/icons-material";

import useAuth  from "../auth-context";
import routes from "../routes";


const Sidebar = ({mode,setMode, onSelect}) => {
  const { session } = useAuth();

  let filteredRoutes = routes.filter(r => !r.needsAuth || !!session );

  return (
    <Box flex={1} p={2} sx={{ display: { xs: "none", sm: "block" } }}>
      <Box position="fixed">
        <List>
          { filteredRoutes.map(r => (
            <ListItem key={r.name} disablePadding>
             <ListItemButton onClick={() => onSelect(r.name)} component="a" href="#home">
               <ListItemIcon>
                 <r.icon />
               </ListItemIcon>
               <ListItemText  primary={r.name} />
             </ListItemButton>
           </ListItem>
          ))}
         <ListItem disablePadding>
            <ListItemButton component="a" href="#simple-list">
              <ListItemIcon>
                <ModeNight />
              </ListItemIcon>
              <Switch onChange={e=>setMode(mode === "light" ? "dark" : "light")}/>
            </ListItemButton>
          </ListItem>
          
        </List>
      </Box>
    </Box>
  );
};

export default Sidebar;
